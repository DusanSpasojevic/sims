// File:    Gender.cs
// Author:  Toshiba
// Created: utorak, 02. jun 2020. 03:09:52
// Purpose: Definition of Enum Gender

using System;

namespace Model
{
   public enum Gender
   {
      male,
      female
   }
}