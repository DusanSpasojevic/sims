// File:    Country.cs
// Author:  Toshiba
// Created: utorak, 02. jun 2020. 03:09:52
// Purpose: Definition of Class Country

using System;

namespace Model
{
   public class Country
   {
      private String name;
      private int id;
      
      public System.Collections.ArrayList city;
      
      /// <summary>
      /// Property for collection of City
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.ArrayList City
      {
         get
         {
            if (city == null)
               city = new System.Collections.ArrayList();
            return city;
         }
         set
         {
            RemoveAllCity();
            if (value != null)
            {
               foreach (City oCity in value)
                  AddCity(oCity);
            }
         }
      }
      
      /// <summary>
      /// Add a new City in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddCity(City newCity)
      {
         if (newCity == null)
            return;
         if (this.city == null)
            this.city = new System.Collections.ArrayList();
         if (!this.city.Contains(newCity))
            this.city.Add(newCity);
      }
      
      /// <summary>
      /// Remove an existing City from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveCity(City oldCity)
      {
         if (oldCity == null)
            return;
         if (this.city != null)
            if (this.city.Contains(oldCity))
               this.city.Remove(oldCity);
      }
      
      /// <summary>
      /// Remove all instances of City from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllCity()
      {
         if (city != null)
            city.Clear();
      }
   
   }
}