// File:    Adress.cs
// Author:  Toshiba
// Created: utorak, 02. jun 2020. 03:09:52
// Purpose: Definition of Class Adress

using System;

namespace Model
{
   public class Adress
   {
      private String streetName;
      private String houseNumber;
      private int id;
      
      public Country country;
      
      /// <summary>
      /// Property for Country
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public Country Country
      {
         get
         {
            return country;
         }
         set
         {
            this.country = value;
         }
      }
      public City city;
      
      /// <summary>
      /// Property for City
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public City City
      {
         get
         {
            return city;
         }
         set
         {
            this.city = value;
         }
      }
   
   }
}