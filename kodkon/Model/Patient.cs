// File:    Patient.cs
// Author:  Toshiba
// Created: utorak, 02. jun 2020. 03:09:52
// Purpose: Definition of Class Patient

using System;

namespace Model
{
   public class Patient
   {
      private int patientID;
      
      public Appointment MakeAnAppointment(String doctorUserName, Appointment appointment, DateTime dateTime)
      {
         throw new NotImplementedException();
      }
      
      public Appointment ChangeAppointment(Appointment appoiitment)
      {
         throw new NotImplementedException();
      }
      
      public Appointment CancelAppointmen(Appointment appointment)
      {
         throw new NotImplementedException();
      }
      
      public List<Appointment> ShowAppointments()
      {
         throw new NotImplementedException();
      }
      
      public List<Doctor> ShowDoctors(Doctor doc)
      {
         throw new NotImplementedException();
      }
      
      public List<Teraphy> ShowTeraphyList(Teraphy teraphy, User user)
      {
         throw new NotImplementedException();
      }
      
      public Teraphy ShowOngoingTeraphy(Teraphy teraphy, User user)
      {
         throw new NotImplementedException();
      }
      
      public Teraphy ChangeOngoingTeraphy(Teraphy teraphy)
      {
         throw new NotImplementedException();
      }
      
      public Boolean AreThereAppointments(Appointment appointment)
      {
         throw new NotImplementedException();
      }
      
      public Boolean AreThereTherapies(Teraphy therapy)
      {
         throw new NotImplementedException();
      }
      
      public Boolean AreThereDocorsAvaliable(Doctor doctoor)
      {
         throw new NotImplementedException();
      }
      
      public User user;
      public System.Collections.ArrayList appointment;
      
      /// <summary>
      /// Property for collection of Appointment
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.ArrayList Appointment
      {
         get
         {
            if (appointment == null)
               appointment = new System.Collections.ArrayList();
            return appointment;
         }
         set
         {
            RemoveAllAppointment();
            if (value != null)
            {
               foreach (Appointment oAppointment in value)
                  AddAppointment(oAppointment);
            }
         }
      }
      
      /// <summary>
      /// Add a new Appointment in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddAppointment(Appointment newAppointment)
      {
         if (newAppointment == null)
            return;
         if (this.appointment == null)
            this.appointment = new System.Collections.ArrayList();
         if (!this.appointment.Contains(newAppointment))
         {
            this.appointment.Add(newAppointment);
            newAppointment.Patient = this;
         }
      }
      
      /// <summary>
      /// Remove an existing Appointment from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveAppointment(Appointment oldAppointment)
      {
         if (oldAppointment == null)
            return;
         if (this.appointment != null)
            if (this.appointment.Contains(oldAppointment))
            {
               this.appointment.Remove(oldAppointment);
               oldAppointment.Patient = null;
            }
      }
      
      /// <summary>
      /// Remove all instances of Appointment from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllAppointment()
      {
         if (appointment != null)
         {
            System.Collections.ArrayList tmpAppointment = new System.Collections.ArrayList();
            foreach (Appointment oldAppointment in appointment)
               tmpAppointment.Add(oldAppointment);
            appointment.Clear();
            foreach (Appointment oldAppointment in tmpAppointment)
               oldAppointment.Patient = null;
            tmpAppointment.Clear();
         }
      }
      public PatientFile patientFile;
   
   }
}