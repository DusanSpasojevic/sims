// File:    Appointment.cs
// Author:  Toshiba
// Created: utorak, 02. jun 2020. 03:09:52
// Purpose: Definition of Class Appointment

using System;

namespace Model
{
   public class Appointment
   {
      private int id;
      private System.DateTime time;
      private System.TimeSpan duration = 15;
      private Boolean finished;
      private list<Doctor> doctor;
      
      public Patient patient;
      
      /// <summary>
      /// Property for Patient
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public Patient Patient
      {
         get
         {
            return patient;
         }
         set
         {
            if (this.patient == null || !this.patient.Equals(value))
            {
               if (this.patient != null)
               {
                  Patient oldPatient = this.patient;
                  this.patient = null;
                  oldPatient.RemoveAppointment(this);
               }
               if (value != null)
               {
                  this.patient = value;
                  this.patient.AddAppointment(this);
               }
            }
         }
      }
   
   }
}