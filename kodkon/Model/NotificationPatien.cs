// File:    NotificationPatien.cs
// Author:  Toshiba
// Created: utorak, 02. jun 2020. 03:09:52
// Purpose: Definition of Class NotificationPatien

using System;

namespace Model
{
   public class NotificationPatien
   {
      private int id;
      private System.DateTime canceledAppointment;
      private System.DateTime newAppointment;
      
      public Patient patient;
      
      /// <summary>
      /// Property for Patient
      /// </summary>
      /// <pdGenerated>Default opposite class property</pdGenerated>
      public Patient Patient
      {
         get
         {
            return patient;
         }
         set
         {
            this.patient = value;
         }
      }
   
   }
}