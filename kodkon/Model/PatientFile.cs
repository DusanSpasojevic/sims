// File:    PatientFile.cs
// Author:  Toshiba
// Created: utorak, 02. jun 2020. 03:09:52
// Purpose: Definition of Class PatientFile

using System;

namespace Model
{
   public class PatientFile
   {
      private int id;
      
      public Patient patient;
      public System.Collections.ArrayList appointmentReport;
      
      /// <summary>
      /// Property for collection of AppointmentReport
      /// </summary>
      /// <pdGenerated>Default opposite class collection property</pdGenerated>
      public System.Collections.ArrayList AppointmentReport
      {
         get
         {
            if (appointmentReport == null)
               appointmentReport = new System.Collections.ArrayList();
            return appointmentReport;
         }
         set
         {
            RemoveAllAppointmentReport();
            if (value != null)
            {
               foreach (AppointmentReport oAppointmentReport in value)
                  AddAppointmentReport(oAppointmentReport);
            }
         }
      }
      
      /// <summary>
      /// Add a new AppointmentReport in the collection
      /// </summary>
      /// <pdGenerated>Default Add</pdGenerated>
      public void AddAppointmentReport(AppointmentReport newAppointmentReport)
      {
         if (newAppointmentReport == null)
            return;
         if (this.appointmentReport == null)
            this.appointmentReport = new System.Collections.ArrayList();
         if (!this.appointmentReport.Contains(newAppointmentReport))
            this.appointmentReport.Add(newAppointmentReport);
      }
      
      /// <summary>
      /// Remove an existing AppointmentReport from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveAppointmentReport(AppointmentReport oldAppointmentReport)
      {
         if (oldAppointmentReport == null)
            return;
         if (this.appointmentReport != null)
            if (this.appointmentReport.Contains(oldAppointmentReport))
               this.appointmentReport.Remove(oldAppointmentReport);
      }
      
      /// <summary>
      /// Remove all instances of AppointmentReport from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllAppointmentReport()
      {
         if (appointmentReport != null)
            appointmentReport.Clear();
      }
   
   }
}