// File:    Role.cs
// Author:  Toshiba
// Created: utorak, 02. jun 2020. 03:09:52
// Purpose: Definition of Enum Role

using System;

namespace Model
{
   public enum Role
   {
      doctor,
      patient,
      warden,
      secretary
   }
}