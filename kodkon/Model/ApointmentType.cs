// File:    ApointmentType.cs
// Author:  Toshiba
// Created: utorak, 02. jun 2020. 03:09:52
// Purpose: Definition of Enum ApointmentType

using System;

namespace Model
{
   public enum ApointmentType
   {
      emergency,
      regular
   }
}